# SPDX-FileCopyrightText: 2024 Gerald Wiese <wiese@gnuhealth.org>
# SPDX-FileCopyrightText: 2024 Leibniz University Hannover
#
# SPDX-License-Identifier: GPL-3.0-or-later

FROM python:3.12-bookworm

RUN apt-get update && apt-get install -y python3-docutils && \
    pip3 install sphinx sphinx-rtd-theme sphinx-toolbox sphinxcontrib-images sphinx-lint sphinx-substitution-extensions
