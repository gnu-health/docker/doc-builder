# Doc Builder

This image is only used to install the depencies for building the GNU Health documentation.

That way we can reduce our CI pipeline from a minute to 5 seconds.

APT package `python3-docutils` is used for running the `rst2html` command to build MyGNUHealth & Thalamus docs.

The Hospital Information System (HIS) docs come with a requirements.txt for Sphinx PyPI packages.

Ansible documentation only needs a subset of those Sphinx PyPI packages of the HIS.
